<?php

/**
 * Set error reporting and display errors settings.  You will want to change these when in production.
 */
ini_set('display_errors', 1);
error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));

/**
 * Setting Default LOCALE
 */
setlocale(LC_ALL, "pl_PL.utf8");
setlocale(LC_TIME, "pl_PL.utf8");
date_default_timezone_set('Europe/Warsaw');

/**
 * Check if in dev state
 */
if(file_exists(DOC_ROOT."_DEVELOPMENT")) define ('DEVELOPMENT',TRUE);


/**
 * The path to the framework core.
 */
define('CORE_PATH', realpath(BASE_PATH.'core').DIRECTORY_SEPARATOR);

/**
 * The path to the outsorce aplication.
 */
define('LIB_PATH', realpath(BASE_PATH.'vendor').DIRECTORY_SEPARATOR);

/**
 * SET include PATH
 */
$path = BASE_PATH. PATH_SEPARATOR;
$path .= CORE_PATH. PATH_SEPARATOR;
$path .= LIB_PATH. PATH_SEPARATOR;
set_include_path($path.get_include_path());


/*
 * SESSION
 */
$SID = session_id();
if(empty($SID)) session_start() or exit(basename(__FILE__).'(): Could not start session'); 