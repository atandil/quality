<?php

namespace SandS;

/**
 * Description of Auth
 *
 * @author Damian
 */
class Auth {

    private $fm;
    private $pracownik;

    public function __construct($fm) {
        $this->fm = $fm;
    }

    /*  public function __get($name)
      {
      if (array_key_exists($name, $this->pracownik)) {
      return $this->pracownik[$name];
      }
      } */

    public function isLogged() {

        if ($_SESSION[logged])
            return TRUE;
        else
            return FALSE;
    }

    public function logout() {

        unset($_SESSION[logged]);
        unset($_SESSION[pracownik]);
        header('Location: /');
    }

    public function login() {
        if ($_SESSION[logged])
            return TRUE;
        $db = $this->fm->getDB();
        $error = FALSE;

        $login = $_POST[username];
        $pass = md5($_POST[password]);
        $file_pass = $_SESSION[pliki][0][md5];
        unset($_SESSION[pliki]);



        //uwierzytelnianie plikiem
        if ($file_pass) {
            $p = $db->getRow("select * from pracownicy where file_pass='$file_pass'");
            if (!$p)
                $error = "Niewłaściwy obrazek/Brak pracownika";
        }
        elseif ($login) {
            $p = $db->getRow("select * from pracownicy where login='$login'");
            if (!$p)
                $error = "Brak pracownika w bazie danych";
            if ($p && $p[pass] != $pass)
                $error = "Niewłaściwe hasło";
        }

        $tpl = $this->fm->getTPL();

        if ($error) {
            $tpl->assign('error', $error);
        } elseif ($p) {
            $pracownik = $p;
            unset($pracownik[pass]);
            unset($pracownik[file_pass]);
            $this->fm->pracownik = $pracownik;
            $this->pracownik = $pracownik;
            $_SESSION[logged] = $pracownik[login];
            $_SESSION[pracownik] = $pracownik;
            return TRUE;
        }


        $tpl->assign('adres', $_SERVER["REQUEST_URI"]);
        $tpl->setTemplate('login.html', 'main');
        $tpl->show();
        exit();

        return FALSE;
    }

    public function loginguest() {
        if ($_SESSION[logged])
            return TRUE;
        $db = $this->fm->getDB();
        $error = FALSE;


        //uwierzytelnianie pytaniami
        if ($_POST) {
            if ($_POST[imie] == 'alex' && $_POST[surowiec] == 'wegiel') {
                $p = $db->getRow("select * from pracownicy where login='gosc'");
                $_SESSION[pracownik] = $p;
                return TRUE;
            }
            else
                $error = 1;
        }

        $tpl = $this->fm->getTPL();

        if ($error) {
            $tpl->assign('error', $error);
        } elseif ($p) {
            $pracownik = $p;
            unset($pracownik[pass]);
            unset($pracownik[file_pass]);
            $this->fm->pracownik = $pracownik;
            $this->pracownik = $pracownik;
            $_SESSION[logged] = $pracownik[login];
            $_SESSION[pracownik] = $pracownik;
            return TRUE;
        }


        $tpl->assign('adres', $_SERVER["REQUEST_URI"]);
        $tpl->setTemplate('pytania.html', 'main');
        $tpl->show();
        exit();

        return FALSE;
    }

}
