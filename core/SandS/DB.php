<?php

/**
 * Description of DB
 *
 * @author Damian
 */
include_once("Adodb/adodb-exceptions.inc.php");
include_once("Adodb/adodb.inc.php");

try {

    $dsn = 'postgres9://quality:j4k0sc@asternet.pl/quality';  # persist is optional

    $db = ADONewConnection($dsn);  # no need for Connect/PConnect
} catch (exception $e) {

    var_dump($e);

    adodb_backtrace($e->gettrace());
}

