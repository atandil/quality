<?php

namespace SandS;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Config
 *
 * @author Damian
 */
class Config {

    private $config = array();
    private $config_file = 'config/config.xml';
    private $yalm;

    public function __construct($app) {

        if ($app == 'Global')  $dir = BASE_PATH;
        else  $dir = BASE_PATH . 'App/' . $app . '/';
        
        $this->yalm = new Symfony\Component\Yaml\Parser();
        
        $this->config = $this->readConfig($dir . $this->config_file);
        if (DEVELOPMENT) {
            $devconfig = $this->readConfig($dir . 'config/config_dev.yml');
            $this->config = array_merge($this->config, $devconfig);
        }
    }

    /*
     * Funcja pobierajacą konfig z pliku yalm i oddajaca go jako tabela
     * @params string path
     * @return array config
     */
    public function readConfig($path) {

        try {
            $config = $this->yaml->parse(file_get_contents($path));
        } catch (Symfony\Component\Yaml\Exception\ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }

        return $config;
    }

    public function __get($var) {
        return $this->config[$var];
    }

}

