<?php
namespace SandS;
require_once('Smarty/Smarty.class.php');



class MyTPL extends \Smarty {
    /*
     * nazwa aplikacji
     */

    private $app;

    /*
     * konstruktur ustawia tatalogi i aplikacje
     */

    public function __construct($app) {
        parent::__construct();

        $this->cache_dir = BASE_PATH .'tmp/cache';
        $this->compile_dir = BASE_PATH .'tmp/templates_c';
        $this->template_dir = BASE_PATH .'App/' . $app . '/templates';
        $this->app = $app;
    }

    /*
     * parsuj szablon
     * @param string nazwa pliku
     * @param string aplikacja
     */
    public function fetchTemplate($file, $app = NULL) {
        
        if(!$app) $app=$this->app;

        if (file_exists(BASE_PATH . 'App/' . $app . '/templates/' . $file))
            $out = $this->fetch($file);
        else
            $out = $this->fetch('default.html');

        return $out;
    }

    /*
     * przypisz sparsowany plik do zmiennej
     * @param string nazwa pliku
     * @param string wartosc
     * @param string aplikacja
     */
    public function setTemplate($file, $var,$app=NULL) {
        if(!$app) $app=$this->app;
        if (file_exists(BASE_PATH . 'App/' . $app . '/templates/' . $file))
            $out = $this->fetch($file);
        else
            $out = $this->fetch('default.html');

        $this->assign($var, $out);
    }

    /*
     * wyswietl szablon
     * @param string nazwa pliku
     * @param string aplikacja
     */
    public function show($design = NULL,$app=NULL) {
        if(!$app) $app=$this->app;
        if ($design)
            $this->display($design);
        else
            $this->display('design.html');
    }

}

