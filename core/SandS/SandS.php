<?php

namespace SandS;

//use \Symfony as Symfony;
/**
 * Głowna klasa frameworka
 *
 * @author Damian Barczyk
 * @version 1.0.0
 */
class SandS {

    /**
     *
     * @var array
     * Zmienna trzymająca konfig 
     */
    private $config;

    /**
     *
     * @var array
     * Zmienna trzymająca konfig 
     */
    private $auth;
    private $db;
    private $tpl;
    public $pracownik=array();

    const DEFAULT_APPLICATION = "Main";
    const DEFAULT_CONTROLLER = "IndexController";
    const DEFAULT_ACTION = "index";

    private $application = self::DEFAULT_APPLICATION;
    private $controller = self::DEFAULT_CONTROLLER;
    private $action = self::DEFAULT_ACTION;
    //private $params = array();
    private $apps_list = array('admin');

    public function __construct(array $options = array()) {
        if (empty($options)) {
            $this->parseUri();
        } else {
            if (isset($options["application"])) {
                $this->setApplication($options["application"]);
            }
            if (isset($options["controller"])) {
                $this->setController($options["controller"]);
            }
            if (isset($options["action"])) {
                $this->setAction($options["action"]);
            }
            if (isset($options["params"])) {
                $this->setParams($options["params"]);
            }
        }
    }

    protected function parseUri() {

        if ($_GET[application])
            $application = $_GET[application];
        if ($_GET[controller])
            $controller = $_GET[controller];
        if ($_GET[action])
            $action = $_GET[action];
        if ($_GET[params])
            $params = $_GET[params];

        if (!$controller) {
            $path = trim(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), "/");
            $path = preg_replace('/[^a-zA-Z0-9]\//', "", $path);
            @list($tmp) = explode("/", $path, 2);
            if (isset($tmp)) {

                if (in_array($tmp, $this->apps_list))
                    @list($application, $controller, $action, $params) = explode("/", $path, 4);
                else
                    @list($controller, $action, $params) = explode("/", $path, 3);
            }
        }


        if (!empty($application)) {
            $this->setApplication($application);
        }
        if (!empty($controller)) {
            $this->setController($controller);
        }
        if (!empty($action)) {
            $this->setAction($action);
        }
        if (!empty($params)) {
            $this->setParams(explode("/", $params));
        }
    }

    public function setApplication($application) {
        $this->application = $application;
        return $this;
    }

    public function setController($controller) {

        //najpierw routing
        if (strtolower($controller) == 'logout') {
            \SandS\Auth::logout();
            exit(0);
        }


        $controller = ucfirst(strtolower($controller)) . "Controller";
        if (!class_exists("App\\" . $this->application . "\\Controller\\" . $controller)) {
            //throw new \InvalidArgumentException(
            //"Kontroler '$controller' nie został zdefiniowany.");
            header('HTTP/1.0 404 Not Found');
            echo "<h1>404 Not Found</h1>";
            echo "Nie znaleziono kontrolera!.";
            exit();
        }
        $this->controller = $controller;
        return $this;
    }

    public function setAction($action) {
        $reflector = new \ReflectionClass("App\\" . $this->application . "\\Controller\\" . $this->controller);
        if (!$reflector->hasMethod($action)) {
            //throw new \InvalidArgumentException(
            //"Metoda '$action' nie została zdefiniowana.");
            $action = 'index';
        }
        $this->action = $action;
        return $this;
    }

    public function setParams(array $params) {
        $this->params = $params;
        return $this;
    }

    public function run() {
        $GLOBALS['app'] = $this->application;
        $controller = "App\\" . $this->application . "\\Controller\\" . $this->controller;
        call_user_func_array(array(new $controller($this), $this->action), array($this->params));
    }

    public function loadModel($modelname) {

        $modelfile = 'App/' . $this->application . '/Model/' . $modelname . '.php';
        //echo BASE_PATH.$modelfile;
        //if(!file_exists(BASE_PATH.$modelfile));
        //   throw new \InvalidArgumentException("Model '$modelname' nie został zdefiniowany.");

        $model = "App\\" . $this->application . "\\Model\\" . $modelname;

        return new $model($this);
    }

    /**
     * Zwraca objekt parametrami
     * @param string Nazwa aplikacji
     * @return array tablica z konfigiem
     */
    function getConfig($app = FALSE) {

        if (!$app)
            $app = 'Global';

        if (!$this->config[$app]) {

            $this->config[$app] = new \SandS\Config($app);
        }

        return $this->config[$app];
    }

    /**
     * Zwraca tablice z parametrami
     * @param string Nazwa aplikacji
     * @return array tablica z konfigiem
     */
    function getTPL($app = FALSE) {

        if (!$app)
            $app = 'Main';

        if (!$this->tpl) {

            $this->tpl = new \SandS\MyTPL($app);
            $this->tpl->assign('request_url', $_SERVER["REQUEST_URI"]);
        }

        return $this->tpl;
    }
 
    function getAuth() {

        if (!$this->auth) {

            $this->auth = new \SandS\Auth($this);
        }

        return $this->auth;
    }

    function getDB() {

        if (!$this->db) {

            include 'core/SandS/DB.php';
            $this->db = $db;
        }

        return $this->db;
    }

    function getPrac($name = NULL) {
        $pracownik=$this->pracownik;
        if (array_key_exists($name, $pracownik)) {
            return $pracownik[$name];
        }
        else
            return $this->pracownik;
    }

}

?>
