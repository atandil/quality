<?php

/**
 * Starting autoloader
 */

require_once LIB_PATH.'Symfony/Component/ClassLoader/UniversalClassLoader.php';
 
use Symfony\Component\ClassLoader\UniversalClassLoader;
 
$loader = new UniversalClassLoader();

$loader->useIncludePath(true);
/**
 * Smarter Namespace i Core dir
 */
$loader->registerNamespace('SandS', CORE_PATH);


/**
 * Register autoload functions
 */
$loader->register();
