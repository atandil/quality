<?php
namespace App\Main\Model;

/**
 * Description of Fronted
 *
 * @author Damian
 */
class Mail extends \SandS\DefaultModel {
    
    function send($subject,$text,$from,$to) {
        
        if(!$subject) $subject="Dodano niezgodnosc";
        
         try {
                // minimal requirements to be set
                $dummy = new \SandS\Mailer();
                $dummy->setFrom("E-Działania", "edzialania@asternet.pl");
                $dummy->addRecipient("Damian", "damian@asternet.pl");
                $dummy->fillSubject($subject);
                $dummy->fillMessage($text);

                // now we send it!
                $dummy->send();
            } catch (Exception $e) {
                echo $e->getMessage();
                exit(0);
            }
        
    }
    
    function mailTPL($template,$data,$subject,$from,$to) {
        $this->send($subject, $text, $from, $to);
    }
    
    
    
}
