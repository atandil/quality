<?php

namespace App\Main\Model;

/**
 * Description of Fronted
 *
 * @author Damian
 */
class Pliki extends \SandS\DefaultModel {

    function save($file_name, $path) {
        $path=BASE_PATH.'public/pliki/'.$path;
        
          if (!is_dir($path))
            mkdir($path, 0755,TRUE);

        //$plik_nazwa = $path . $file_name;
        //poprawiam nazwe
        $new_file_name = $this->nazwaPlik($file_name);
        $new_file_name = $this->filelimitmid($new_file_name, 45);
        $plik_nazwa = $path . $new_file_name;

        if (file_exists($plik_nazwa))
           $plik_nazwa=$path.rand().$new_file_name;
        $old=BASE_PATH.'tmp/upload/'.$file_name;
        rename($old,$plik_nazwa);
        //unlink($old);
       
        return $new_file_name;
        
    }

    function nazwaPlik($file) {
        $plik_nazwa = strtolower($file);

        $plik_nazwa = str_replace(" ", "_", $plik_nazwa);
        $plik_nazwa = str_replace("(", "_", $plik_nazwa);
        $plik_nazwa = str_replace(")", "_", $plik_nazwa);
        $plik_nazwa = str_replace("&", "_", $plik_nazwa);
        $plik_nazwa = str_replace("%", "_", $plik_nazwa);
        $plik_nazwa = str_replace("!", "_", $plik_nazwa);
        $plik_nazwa = str_replace("*", "_", $plik_nazwa);
        $plik_nazwa = str_replace("@", "_", $plik_nazwa);
        $plik_nazwa = str_replace("^", "_", $plik_nazwa);
        $plik_nazwa = str_replace("$", "_", $plik_nazwa);

        $plik_nazwa = str_replace("ą", "a", $plik_nazwa);

        $plik_nazwa = str_replace("Ą", "a", $plik_nazwa);

        $plik_nazwa = str_replace("ć", "c", $plik_nazwa);

        $plik_nazwa = str_replace("Ć", "c", $plik_nazwa);

        $plik_nazwa = str_replace("ę", "e", $plik_nazwa);

        $plik_nazwa = str_replace("Ę", "e", $plik_nazwa);

        $plik_nazwa = str_replace("ł", "l", $plik_nazwa);

        $plik_nazwa = str_replace("Ł", "l", $plik_nazwa);

        $plik_nazwa = str_replace("ń", "n", $plik_nazwa);

        $plik_nazwa = str_replace("Ń", "n", $plik_nazwa);

        $plik_nazwa = str_replace("ó", "o", $plik_nazwa);

        $plik_nazwa = str_replace("Ó", "o", $plik_nazwa);

        $plik_nazwa = str_replace("ś", "s", $plik_nazwa);

        $plik_nazwa = str_replace("Ś", "s", $plik_nazwa);

        $plik_nazwa = str_replace("ż", "z", $plik_nazwa);

        $plik_nazwa = str_replace("Ż", "z", $plik_nazwa);

        $plik_nazwa = str_replace("ź", "z", $plik_nazwa);

        $plik_nazwa = str_replace("Ź", "z", $plik_nazwa);

        return $plik_nazwa;
    }

//////////////////////////
    function limitchrmid($value, $lenght) {
        if (strlen($value) >= $lenght) {
            $lenght_max = ($lenght / 2) - 3;
            $start = strlen($value) - $lenght_max;
            $limited = substr($value, 0, $lenght_max);
            $limited.= " ... ";
            $limited.= substr($value, $start, $lenght_max);
        } else {
            $limited = $value;
        }
        return $limited;
    }

/////////////////////////
    function filelimitmid($value, $lenght) {
        if (strlen($value) >= $lenght) {
            $ext = pathinfo($value, PATHINFO_EXTENSION);
            $value = pathinfo($value, PATHINFO_FILENAME);
            $lenght_max = ($lenght / 2) - 3;
            $start = strlen($value) - $lenght_max;
            $limited = substr($value, 0, $lenght_max);
            $limited.= "_";
            $limited.= substr($value, $start, $lenght_max) . "." . $ext;
        } else {
            $limited = $value;
        }
        return $limited;
    }

}
