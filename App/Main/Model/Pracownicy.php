<?php

namespace App\Main\Model;

/**
 * Description of Fronted
 *
 * @author Damian
 */
class Pracownicy extends \SandS\DefaultModel {

    function get($id_pracownika) {
        $db = $this->fm->getDB();
        $where = "id_pracownika=$id_pracownika";
        $sql = "select * from pracownicy_full where $where";
        $sprawa = $db->getRow($sql);

        return $sprawa;
    }
    //////////////////////////////////////////////////

    /**
     * Dodawanie pracownicy
     *
     * @param   array $form  tabela z parametrami
     * @return  integer id_pracownika or false
     * 
     */
    function dodaj($form) {
        $db = $this->fm->getDB();
        // var_dump($db);

       // $form[id_zglaszajacego] = $_SESSION[pracownik][id_pracownika];

        try {

            $db->autoExecute('pracownicy', $form, INSERT);
            return $db->Insert_ID();
        } catch (exception $e) {

            var_dump($e);

            adodb_backtrace($e->gettrace());
        }
    }

    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function update($form, $where) {
        $db = $this->fm->getDB();

        if (is_numeric($where)) {
            $where = "id_pracownika=$where";
        }

        if($form[pass]=='') unset($form[pass]);
        else $form[pass] = md5($form[pass]);
        //print_r($form);

        try {
            $db->autoExecute('pracownicy', $form, UPDATE, $where);
            //return $db->Insert_ID();
        } catch (exception $e) {
            var_dump($e);
            adodb_backtrace($e->gettrace());
        }
    }
    ////////////////////////////////////////////////
    function dzialy($where=NULL,$order=NULL) {
         $db = $this->fm->getDB();
         $sql = "SELECT * from dzialy_full ";


        if ($where)
            $sql .= "WHERE $where ";

        if ($order)
            $sql .= "ORDER BY $order";
        else
            $sql .= "ORDER BY zaklad,dzial";
        
        if ($_SESSION['debug'] == 2)
            echo $sql;

        return $db->getAll($sql);
    }
	
	
	 ////////////////////////////////////////////////
    function zaklady($where=NULL,$order=NULL) {
         $db = $this->fm->getDB();
         $sql = "SELECT * from zaklady ";


        if ($where)
            $sql .= "WHERE $where ";

        if ($order)
            $sql .= "ORDER BY $order";
        else
            $sql .= "ORDER BY id_zakladu";
        
        if ($_SESSION['debug'] == 2)
            echo $sql;

        return $db->getAll($sql);
    }
    
    function obszary($where=NULL,$order=NULL) {
         $db = $this->fm->getDB();
         $sql = "SELECT * from obszary_full ";


        if ($where)
            $sql .= "WHERE $where ";

        if ($order)
            $sql .= "ORDER BY $order";
        else
            $sql .= "ORDER BY zaklad,dzial";
        
        if ($_SESSION['debug'] == 2)
            echo $sql;

        return $db->getAll($sql);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    function listaSimple($where,$order) {
         $db = $this->fm->getDB();
         $sql = "SELECT * from pracownicy ";


        if ($where)
            $sql .= "WHERE $where ";

        if ($order)
            $sql .= "ORDER BY $order";
        else
            $sql .= "ORDER BY nazwisko_p,imie_p";
        
        if ($_SESSION['debug'] == 2)
            echo $sql;

        return $db->getAll($sql);
    }
    
    function lista($what = '*', $where = '', $order = '') {
        $db = $this->fm->getDB();

        if (!$what)
            $what = "*";

        $sql = "SELECT $what from pracownicy_full ";


        if ($where)
            $sql .= "WHERE $where ";

        if ($order)
            $sql .= "ORDER BY $order";
        else
            $sql .= "ORDER BY nazwisko_p,imie_p";

        //echo $sql; exit(0);
        if ($_SESSION['debug'] == 2)
            echo $sql;

        try {
            $pracownicy = $db->getAll($sql);
            if ($_SESSION['debug'] == 2)
                print_r($pracownicy);
            return $pracownicy;
            //return $db->Insert_ID();
        } catch (exception $e) {
            var_dump($e);
            adodb_backtrace($e->gettrace());
        }
    }
    
    function listaWydzialow($what = '*', $where = '', $order = '') {
        $db = $this->fm->getDB();

        if (!$what)
            $what = "*";

        $sql = "SELECT $what from wydzialy ";


        if ($where)
            $sql .= "WHERE $where ";

        if ($order)
            $sql .= "ORDER BY $order";
        else
            $sql .= "ORDER BY wydzial";

        //echo $sql; exit(0);
        if ($_SESSION['debug'] == 2)
            echo $sql;

        try {
            $pracownicy = $db->getAll($sql);
            if ($_SESSION['debug'] == 2)
                print_r($pracownicy);
            return $pracownicy;
            //return $db->Insert_ID();
        } catch (exception $e) {
            var_dump($e);
            adodb_backtrace($e->gettrace());
        }
    }

}
