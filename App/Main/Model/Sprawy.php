<?php

namespace App\Main\Model;

/**
 * Description of Fronted
 *
 * @author Damian
 */
class Sprawy extends \SandS\DefaultModel {

    function nextNR() {
        $db = $this->fm->getDB();
        return $db->getOne("SELECT max((substring(numer from '[0-9]+$'))::integer)+1 from sprawy");
    }

    function get($id_sprawy) {
        $db = $this->fm->getDB();
        $where = "id_sprawy=$id_sprawy";
        $sql = "select * from sprawy_full where $where";
        $sprawa = $db->getRow($sql);

        return $sprawa;
    }

    function getKoment($id_sprawy, $krok) {
        $db = $this->fm->getDB();
        $where = "id_sprawy=$id_sprawy ";

        switch ($krok) {
            case 1:
                $where.="and id_stanu<30";
                break;
            case 2:
                $where.="and id_stanu>=30 and id_stanu<60";
                break;
            case 3:
                $where.="and id_stanu>=60 and id_stanu<70";
                break;
            case 4:
                $where.="and id_stanu>=70 and id_stanu<90";
                break;
        }

        $sql = "select * from komentarze where $where";
        $out = $db->getAll($sql);

        return $out;
    }

    function getPliki($id_sprawy) {
        $db = $this->fm->getDB();
        $where = "id_sprawy=$id_sprawy";
        $sql = "select * from pliki where $where";
        $out = $db->getAll($sql);

        foreach ($out as $k => $v) {
            //poprawiam mime
            $path_parts = pathinfo($v[plik]);
            $ext = strtolower($path_parts["extension"]);
            $show = false;
            // Determine Content Type
            switch ($ext) {
                case "pdf": $ctype = "application/pdf";
                    $show = true;
                    break;
                case "exe": $ctype = "application/octet-stream";
                    break;
                case "zip": $ctype = "application/zip";
                    break;
                case "doc": $ctype = "application/msword";
                    break;
                case "rtf": $ctype = "application/msword";
                    break;
                case "xls": $ctype = "application/vnd.ms-excel";
                    break;
                case "ppt": $ctype = "application/vnd.ms-powerpoint";
                    break;
                case "gif": $ctype = "image/gif";
                    $show = true;
                    break;
                case "png": $ctype = "image/png";
                    $show = true;
                    break;
                case "jpeg":
                case "jpg": $ctype = "image/jpeg";
                    $show = true;
                    break;
                default: $ctype = "application/force-download";
            }
            $out[$k][mime] = $ctype;
            $out[$k][show] = $show;

        }
        
          //print_r($out);
          return $out;
    }

    function getDzialania($id_sprawy, $typ_dzialania = 1) {
        $db = $this->fm->getDB();
        $where = "id_sprawy=$id_sprawy ";
        if ($typ_dzialania)
            $where.="and typ_dzialania=$typ_dzialania";
        $sql = "select *  from dzialania where $where";
        $out = $db->getAll($sql);

        return $out;
    }

    function getDane($id_sprawy) {
        $db = $this->fm->getDB();
        $where = "id_sprawy=$id_sprawy ";
        $sql = "select *  from sprawy_dane where $where";
        $out = $db->getRow($sql);

        return $out;
    }

    function getDzialanie($id_dzialania) {
        $db = $this->fm->getDB();
        $where = "id_dzialania=$id_dzialania ";
        $sql = "select *  from dzialania where $where";
        $out = $db->getRow($sql);

        return $out;
    }

    function historia($id_sprawy, $akcja, $akcja_full = null, $id_pracownika = null, $czas = null) {
        $db = $this->fm->getDB();
        if (!$id_pracownika)
            $id_pracownika = $_SESSION[pracownik][id_pracownika];

        $form[id_sprawy] = $id_sprawy;
        $form[id_pracownika] = $id_pracownika;
        $form[akcja] = $akcja;
        if ($akcja_full)
            $form[akcja_full] = $akcja_full;
        if ($czas)
            $form[czas] = $czas;

        $db->autoExecute('sprawy_historia', $form, INSERT);
    }

    //////////////////////////////////////////////////

    /**
     * Dodawanie sprawy
     *
     * @param   array $form  tabela z parametrami
     * @return  integer id_sprawy or false
     * 
     */
    function dodaj($form) {
        $db = $this->fm->getDB();
        // var_dump($db);

        $form[numer] = date('Y') . '/' . $this->nextNR();
        $form[id_zglaszajacego] = $_SESSION[pracownik][id_pracownika];

        try {

            $db->autoExecute('sprawy', $form, INSERT);
            $id_sprawy = $db->Insert_ID();
            $this->historia($id_sprawy, 'Dodanie sprawy', serialize($form));
            return $id_sprawy;
        } catch (exception $e) {

            var_dump($e);

            adodb_backtrace($e->gettrace());
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function dodajPliki($id_sprawy, $id_stanu, $pliki) {
        $db = $this->fm->getDB();
        $filemod = new Pliki($this->fm);

        if ($pliki && is_array($pliki))
            foreach ($pliki as $pl) {
                $plik[id_sprawy] = $id_sprawy;
                $plik[id_stanu] = $id_stanu;
                $path = $id_sprawy . '/' . date("Y-m-d") . '/';
                $plik[plik] = $filemod->save($pl[name], $path);
                $plik[katalog] = $path;
                $plik[md5] = $pl[md5];
                $plik[id_pracownika] = $_SESSION[pracownik][id_pracownika];
                try {
                    $db->autoExecute('pliki', $plik, INSERT);
                    $this->historia($id_sprawy, 'Dodanie pliku', serialize($plik));
                    //return $db->Insert_ID();
                } catch (exception $e) {
                    var_dump($e);
                    adodb_backtrace($e->gettrace());
                }
            }
            //exit();
    }

    function dodajKoments($id_sprawy, $id_stanu, $komentarze) {
        if ($komentarze && is_array($komentarze))
            foreach ($komentarze as $komentarz) {
                //$komentarz[id_sprawy] = $id_sprawy;
                //$komentarz[id_stanu] = $id_stanu;
                $this->dodajKoment($id_sprawy, $id_stanu, $komentarz);
            }
    }

    function dodajDane($id_sprawy, $dane) {
        $db = $this->fm->getDB();
        $dane[id_sprawy] = $id_sprawy;
        try {
            $db->autoExecute('sprawy_dane', $dane, INSERT);
            //return $db->Insert_ID();
        } catch (exception $e) {
            var_dump($e);
            adodb_backtrace($e->gettrace());
        }
    }

    function dodajKoment($id_sprawy, $id_stanu, $komentarz) {
        $db = $this->fm->getDB();
        $komentarz[id_sprawy] = $id_sprawy;
        $komentarz[id_stanu] = $id_stanu;
        try {
            $db->autoExecute('komentarze', $komentarz, INSERT);
            //return $db->Insert_ID();
        } catch (exception $e) {
            var_dump($e);
            adodb_backtrace($e->gettrace());
        }
    }

    function dodajDzialanie($id_sprawy, $dzialanie) {
        $db = $this->fm->getDB();
        if ($dzialanie && is_array($dzialanie)) {
            $dzialanie[id_sprawy] = $id_sprawy;
            try {
                $db->autoExecute('dzialania', $dzialanie, INSERT);
                $this->historia($id_sprawy, 'Dodanie działania', serialize($dzialanie));
                //return $db->Insert_ID();
            } catch (exception $e) {
                var_dump($e);
                adodb_backtrace($e->gettrace());
            }
        }
    }

    function updateDzialanie($dzialanie, $id_dzialania) {
        $db = $this->fm->getDB();
        $where = "id_dzialania=$id_dzialania";
        if ($dzialanie && is_array($dzialanie)) {
            //$dzialanie[id_sprawy] = $id_sprawy;
            try {
                $db->autoExecute('dzialania', $dzialanie, UPDATE, $where);
                //$this->historia($id_sprawy, 'Dodanie działania', serialize($dzialanie));
                //return $db->Insert_ID();
            } catch (exception $e) {
                var_dump($e);
                adodb_backtrace($e->gettrace());
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function update($form, $where) {
        $db = $this->fm->getDB();

        if (is_numeric($where)) {
            $id_sprawy = $where;
            $where = "id_sprawy=$where";
        }


        try {
            $db->autoExecute('sprawy', $form, UPDATE, $where);
            $this->historia($id_sprawy, 'Aktualizacja sprawy', serialize($form));
            //return $db->Insert_ID();
        } catch (exception $e) {
            var_dump($e);
            adodb_backtrace($e->gettrace());
        }
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function przyjmij($where) {
        $db = $this->fm->getDB();

        if (is_numeric($where)) {
            $id_sprawy = $where;
            $where = "id_sprawy=$where";
        }


        try {
            $db->query("UPDATE SPRAWY set id_stanu=30,numer=numer || '/' || (SELECT symbol_w from wydzialy where wydzialy.id_wydzialu=sprawy.id_wydzialu) WHERE $where");
            //return $db->Insert_ID();
        } catch (exception $e) {
            var_dump($e);
            adodb_backtrace($e->gettrace());
        }
    }

    function odrzuc($where) {
        $db = $this->fm->getDB();

        if (is_numeric($where)) {
            $id_sprawy = $where;
            $where = "id_sprawy=$where";
        }


        try {
            $db->query("UPDATE SPRAWY set id_stanu=20 WHERE $where");
            //return $db->Insert_ID();
        } catch (exception $e) {
            var_dump($e);
            adodb_backtrace($e->gettrace());
        }
    }

    function stan($where, $stan) {

        $db = $this->fm->getDB();

        if (is_numeric($where)) {
            $id_sprawy = $where;
            $where = "id_sprawy=$where";
        }


        try {
            $db->query("UPDATE SPRAWY set id_stanu=$stan WHERE $where");
            //return $db->Insert_ID();
        } catch (exception $e) {
            var_dump($e);
            adodb_backtrace($e->gettrace());
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*
     * Funcja lista
     * @param what
     * @param where
     * @param order
     * @param all - czy pokazujemy wszytsie niezaleznie od roli
     */
    function lista($what = '*', $where = '', $order = '', $all = false) {
        $db = $this->fm->getDB();

        if (!$what)
            $what = "*";

        $sql = "SELECT $what from sprawy_full ";

        $sql .= "WHERE TRUE ";
        if ($where)
            $sql .= "AND ($where) ";
        if (!$all && ($_SESSION[pracownik][rola] == 'pracownik' || $_SESSION[pracownik][rola] == 'kierownik'))
            $sql.='AND (id_wydzialu=' . $_SESSION[pracownik][id_wydzialu] . ')';

        if ($order)
            $sql .= "ORDER BY $order";
        else
            $sql .= "ORDER BY id_sprawy DESC";

        //echo $sql; exit(0);
        if ($_SESSION['debug'] == 2)
            echo $sql;

        try {
            $sprawy = $db->getAll($sql);
            if ($_SESSION['debug'] == 2)
                print_r($sprawy);
            return $sprawy;
            //return $db->Insert_ID();
        } catch (exception $e) {
            var_dump($e);
            adodb_backtrace($e->gettrace());
        }
    }

    function count($where = '', $all = false) {
        $db = $this->fm->getDB();


        $sql = "SELECT count(*) from sprawy_full ";

        $sql .= "WHERE TRUE ";
        if ($where)
            $sql .= "AND ($where) ";
        if (!$all && ($_SESSION[pracownik][rola] == 'pracownik' || $_SESSION[pracownik][rola] == 'kierownik'))
            $sql.='AND (id_wydzialu=' . $_SESSION[pracownik][id_wydzialu] . ')';


        //echo $sql; exit(0);
        if ($_SESSION['debug'] == 2)
            echo $sql;

        try {
            $sprawy = $db->getOne($sql);
            if ($_SESSION['debug'] == 2)
                print_r($sprawy);
            return $sprawy;
            //return $db->Insert_ID();
        } catch (exception $e) {
            var_dump($e);
            adodb_backtrace($e->gettrace());
        }
    }

}

