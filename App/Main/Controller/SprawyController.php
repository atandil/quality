<?php

namespace App\Main\Controller;

/**
 * Description of NiezgodnosciController
 *
 * @author Damian
 */
class SprawyController {

    private $fm;

    public function __construct($fm) {
        $this->fm = $fm;
    }

    function index() {

        $fc = new Fronted($this->fm);
        $fc->auth();


        $tpl = $this->fm->getTPL();

        $tpl->setTemplate('start.html', 'main');
        $tpl->show();
    }

    function nowaNiezgodnosc() {
        $fc = new Fronted($this->fm);
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        $modprac = $this->fm->loadModel('Pracownicy');
        //var_dump($modsprawy);
        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('krok', 1);

        $niezgodnosc = $_POST[niezgodnosc];



        if ($niezgodnosc) {
            //print_r($niezgodnosc); exit();
            //print_r($_SESSION);
            $niezgodnosc[id_stanu] = 10;
            $id_sprawy = $modsprawy->dodaj($niezgodnosc);
            $modsprawy->dodajKoments($id_sprawy, $niezgodnosc[id_stanu], $_SESSION[komentarze]);
            $modsprawy->dodajPliki($id_sprawy, $niezgodnosc[id_stanu], $_SESSION[pliki]);
            header('Location: /index/start');
        } else {
            unset($_SESSION[pliki]);
            unset($_SESSION[komentarze]);
            $niezgodnosc[numer] = date('Y') . '/' .$modsprawy->nextNR();
            $niezgodnosc[data_zgloszenia] = date("Y-m-d");

            $tpl = $this->fm->getTPL();
                        $dzialy = $modprac->dzialy();
            $tpl->assign('dzialy', $dzialy);
            //print_r($dzialy);
            $kierownicy = $modprac->lista('*', "rola='kierownik'", 'id_zakladu,wydzial,nazwisko_p');
            $tpl->assign('kierownicy', $kierownicy);
            $tpl->assign('niezgodnosc', $niezgodnosc);
            $tpl->setTemplate('niezgodnosc.html', 'main');
            $tpl->show();
        }
        //$fc->show('niezgodnosc.html');
    }

    function nowaPNiezgodnosc() {
        $fc = new Fronted($this->fm);
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        $modprac = $this->fm->loadModel('Pracownicy');
        //var_dump($modsprawy);
        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('krok', 1);

        $niezgodnosc = $_POST[niezgodnosc];



        if ($niezgodnosc) {
            //print_r($niezgodnosc); exit();
            //print_r($_SESSION);
            $niezgodnosc[id_stanu] = 10;
            $niezgodnosc[id_typu] = 2;
            $id_sprawy = $modsprawy->dodaj($niezgodnosc);
            $modsprawy->dodajKoments($id_sprawy, $niezgodnosc[id_stanu], $_SESSION[komentarze]);
            $modsprawy->dodajPliki($id_sprawy, $niezgodnosc[id_stanu], $_SESSION[pliki]);
            header('Location: /index/start');
        } else {
            unset($_SESSION[pliki]);
            unset($_SESSION[komentarze]);
            $niezgodnosc[numer] = date('Y') . '/' .$modsprawy->nextNR();
            $niezgodnosc[data_zgloszenia] = date("Y-m-d");

            $tpl = $this->fm->getTPL();
            $dzialy = $modprac->dzialy();
            $tpl->assign('dzialy', $dzialy);
            $kierownicy = $modprac->lista('*', "rola='kierownik'", 'id_zakladu,wydzial,nazwisko_p');
            $tpl->assign('kierownicy', $kierownicy);
            $tpl->assign('niezgodnosc', $niezgodnosc);
             $tpl->assign('niezgodnosc_p', 1);
            $tpl->setTemplate('p_niezgodnosc.html', 'main');
            $tpl->show();
        }
        //$fc->show('niezgodnosc.html');
    }

    function noweZagrozenie() {
        //$fc = new Fronted($this->fm);
        $auth = $this->fm->getAuth();

        if (!$_SESSION[pracownik])
            $auth->loginguest();


        $modsprawy = $this->fm->loadModel('Sprawy');
        $modprac = $this->fm->loadModel('Pracownicy');
        //var_dump($modsprawy);
        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
       
        $niezgodnosc = $_POST[niezgodnosc];
        $dane = $_POST[dane];

        if ($niezgodnosc) {
            
            //echo "<pre>";;print_r($niezgodnosc); print_r($dane);exit();
            //print_r($_SESSION);
            $niezgodnosc[id_stanu] = 10;
            $niezgodnosc[id_typu] = 3;
            if($niezgodnosc[zabezpieczone]) $niezgodnosc[zabezpieczone]=TRUE;
            $id_sprawy = $modsprawy->dodaj($niezgodnosc);
            if(is_array($dane)) $modsprawy->dodajDane($id_sprawy, $dane);
            $modsprawy->dodajKoments($id_sprawy, $niezgodnosc[id_stanu], $_SESSION[komentarze]);
            $modsprawy->dodajPliki($id_sprawy, $niezgodnosc[id_stanu], $_SESSION[pliki]);
           
            if($_SESSION[logged])
             header('Location: /index/start');
            else  header('Location: /');
        } else {
            unset($_SESSION[pliki]);
            unset($_SESSION[komentarze]);
            $niezgodnosc[numer] = date('Y') . '/' .$modsprawy->nextNR();
            $niezgodnosc[data_zgloszenia] = date("Y-m-d");

            $tpl = $this->fm->getTPL();
            $kierownicy = $modprac->lista('*', "rola='kierownik'", 'id_zakladu,wydzial,nazwisko_p');
            $dzialy = $modprac->dzialy();
            $obszary = $modprac->obszary();
            $tpl->assign('kierownicy', $kierownicy);
            $tpl->assign('obszary', $obszary);
            $tpl->assign('dzialy', $dzialy);
            $tpl->assign('niezgodnosc', $niezgodnosc);
            $tpl->setTemplate('zagrozenie.html', 'main');
            $tpl->show();
        }
        //$fc->show('niezgodnosc.html');
    }

    function moje() {
        $fc = new Fronted($this->fm);
        //$id_typu = $this->fm->params[0];
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        //var_dump($modsprawy);

        $sprawy = $modsprawy->lista('*', "id_zglaszajacego=" . $_SESSION[pracownik][id_pracownika], false, true);

        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('sprawy', $sprawy);



        $fc->show('lista_niezgodnosci.html');
    }
    
    
     function przyjete() {
        $fc = new Fronted($this->fm);
        $id_typu = $this->fm->params[0];
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        //var_dump($modsprawy);
        
        switch ($_SESSION[pracownik][rola]) {
            case 'pelnomocnik':
            $where = "(id_stanu=10 or id_stanu=20)";
                break;
            case 'DSB':
            
             if(id_typu==3) $where = "(id_stanu=10 or id_stanu=20)";
             else $where = "id_stanu=10";
                break;
            default:
                  $where = "id_stanu=10";
                break;
        }
        if ($id_typu)
            $where.=" and id_typu=$id_typu";
        $sprawy = $modsprawy->lista('*', $where);

        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('sprawy', $sprawy);

        $fc->show('lista_niezgodnosci.html');
    }

    function zgloszone() {
        $fc = new Fronted($this->fm);
        $id_typu = $this->fm->params[0];
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        //var_dump($modsprawy);

        $where = "id_stanu=10";
        if ($id_typu)
            $where.=" and id_typu=$id_typu";
        $sprawy = $modsprawy->lista('*', $where);

        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('sprawy', $sprawy);

        $fc->show('lista_niezgodnosci.html');
    }

    function wopracowaniu() {
        $fc = new Fronted($this->fm);
        $id_typu = $this->fm->params[0];
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        //var_dump($modsprawy);

        $where = "id_stanu=30";
        if ($id_typu)
            $where.=" and id_typu=$id_typu";
        $sprawy = $modsprawy->lista('*', $where);

        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('sprawy', $sprawy);

        $fc->show('lista_niezgodnosci.html');
    }

    function sprawdzane() {
        $fc = new Fronted($this->fm);
        $id_typu = $this->fm->params[0];
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        //var_dump($modsprawy);

        $where = "id_stanu=40";
        if ($id_typu)
            $where.=" and id_typu=$id_typu";
        $sprawy = $modsprawy->lista('*', $where);

        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('sprawy', $sprawy);

        $fc->show('lista_niezgodnosci.html');
    }

    function wakceptacji() {
        $fc = new Fronted($this->fm);
        $id_typu = $this->fm->params[0];
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        //var_dump($modsprawy);

        $where = "id_stanu=50";
        if ($id_typu)
            $where.=" and id_typu=$id_typu";
        $sprawy = $modsprawy->lista('*', $where);

        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('sprawy', $sprawy);

        $fc->show('lista_niezgodnosci.html');
    }

    function wrealizacji() {
        $fc = new Fronted($this->fm);
        $id_typu = $this->fm->params[0];
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        //var_dump($modsprawy);

        $where = "id_stanu=60";
        if ($id_typu)
            $where.=" and id_typu=$id_typu";
        $sprawy = $modsprawy->lista('*', $where);

        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('sprawy', $sprawy);

        $fc->show('lista_niezgodnosci.html');
    }

    function oceniane() {
        $fc = new Fronted($this->fm);
        $id_typu = $this->fm->params[0];
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        //var_dump($modsprawy);

        $where = "id_stanu=70";
        if ($id_typu)
            $where.=" and id_typu=$id_typu";
        $sprawy = $modsprawy->lista('*', $where);

        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('sprawy', $sprawy);

        $fc->show('lista_niezgodnosci.html');
    }

    function zakonczone() {
        $fc = new Fronted($this->fm);
        $id_typu = $this->fm->params[0];
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        //var_dump($modsprawy);

        $where = "id_stanu=80";
        if ($id_typu)
            $where.=" and id_typu=$id_typu";
        $sprawy = $modsprawy->lista('*', $where);

        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('sprawy', $sprawy);

        $fc->show('lista_niezgodnosci.html');
    }

    function wszystkie() {
        $fc = new Fronted($this->fm);
        $id_typu = $this->fm->params[0];
        $fc->auth();
        $modsprawy = $this->fm->loadModel('Sprawy');
        //var_dump($modsprawy);
        //print_r($this->fm->params);
        if ($id_typu)
            $where = " id_typu=$id_typu";
        $sprawy = $modsprawy->lista('*', $where);

        //var_dump($_POST); exit(0);
        $tpl = $this->fm->getTPL();
        $tpl->assign('sprawy', $sprawy);

        $fc->show('lista_niezgodnosci.html');
    }

}