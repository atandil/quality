<?php

namespace App\Main\Controller;

/**
 * Description of NiezgodnosciController
 *
 * @author Damian
 */
class SprawaController {

    private $fm;

    public function __construct($fm) {
        $this->fm = $fm;
    }

    function index() {

        $fc = new Fronted($this->fm);
        $fc->auth();

        $modsprawy = $this->fm->loadModel('Sprawy');
        $modprac = $this->fm->loadModel('Pracownicy');
        //print_r($this->fm->params);
        $tpl = $this->fm->getTPL();

        $id_sprawy = $this->fm->params[0];
        $krok = 1;

        ///////////////////////////////////////

        $niezgodnosc = $_POST[niezgodnosc];

        if ($niezgodnosc) {
            // print_r($_REQUEST); exit();
            //print_r($_SESSION);
            if ($niezgodnosc[przdziel])
                $niezgodnosc[id_stanu] = 10;
            $modsprawy->update($niezgodnosc, $id_sprawy);
        }

        //////////////////////////////////////

        $sprawa = $modsprawy->get($id_sprawy);
        $tpl->assign('niezgodnosc', $sprawa);

        //komentarze
        $tpl->assign('komentarze', $modsprawy->getKoment($id_sprawy, 1));
        $komentarze1 = $tpl->fetch('komentarze_lista_krok.html');
        $tpl->assign('komentarze1', $komentarze1);

        $tpl->assign('komentarze', $modsprawy->getKoment($id_sprawy, 2));
        $komentarze2 = $tpl->fetch('komentarze_lista_krok.html');
        $tpl->assign('komentarze2', $komentarze2);

        $tpl->assign('komentarze', $modsprawy->getKoment($id_sprawy, 3));
        $komentarze3 = $tpl->fetch('komentarze_lista_krok.html');
        $tpl->assign('komentarze3', $komentarze3);

        $tpl->assign('komentarze', $modsprawy->getKoment($id_sprawy, 4));
        $komentarze4 = $tpl->fetch('komentarze_lista_krok.html');
        $tpl->assign('komentarze4', $komentarze4);

        //pliki
        $pliki1 = $modsprawy->getPliki($id_sprawy);
        //print_r($pliki1);
        $tpl->assign('pliki1', $pliki1);
        
         //dodatkopwe dane
        $dane = $modsprawy->getDane($id_sprawy);
        $tpl->assign('dane', $dane);

        //Dzialania
        $ocena = TRUE;
        $dzialania = $modsprawy->getDzialania($id_sprawy);
        $tpl->assign('rand', rand(1, 900));
        $tpl->assign('dzialania', $dzialania);
        $dzialanian = $tpl->fetch('lista_dzialan.html');
        $tpl->assign('dzialanian', $dzialanian);
        $dzialanian_rel = $tpl->fetch('lista_dzialan_realizacja.html');
        //var_dump($dzialanian_rel);
        $tpl->assign('dzialanian_rel', $dzialanian_rel);
        foreach ($dzialania as $d)
            if (!$d[czas_f])
                $ocena = FALSE;



        $dzialania = $modsprawy->getDzialania($id_sprawy, 2);
        $tpl->assign('rand', rand(1, 900));
        $tpl->assign('dzialania', $dzialania);
        $dzialaniak = $tpl->fetch('lista_dzialan.html');
        $tpl->assign('dzialaniak', $dzialaniak);
        $dzialaniak_rel = $tpl->fetch('lista_dzialan_realizacja.html');
        $tpl->assign('dzialaniak_rel', $dzialaniak_rel);
        foreach ($dzialania as $d)
            if (!$d[czas_f])
                $ocena = FALSE;

        $tpl->assign('ocena', $ocena);

        $kierownicy = $modprac->lista('*', "rola='kierownik'", 'id_zakladu,wydzial,nazwisko_p');
        $tpl->assign('kierownicy', $kierownicy);


        if ($sprawa[id_stanu] > 20)
            $krok = 2;
        if ($sprawa[id_stanu] > 50)
            $krok = 3;
        if ($sprawa[id_stanu] > 60)
            $krok = 4;
        $tpl->assign('krok', $krok);
        if ($sprawa[id_typu] == 3) {
            $tpl->assign('active', $krok-1); 
            $tpl->setTemplate('zagrozenie.html', 'main');
        }
        elseif ($sprawa[id_typu] == 2) {
            $tpl->assign('active', $krok-1); 
            $tpl->setTemplate('p_niezgodnosc.html', 'main');
        }
        else {
            $tpl->assign('active', $krok - 1);
            $tpl->setTemplate('niezgodnosc.html', 'main');
        }

        $tpl->show();
    }

    function akcja() {

        $fc = new Fronted($this->fm);
        $fc->auth();

        $modsprawy = $this->fm->loadModel('Sprawy');
        //print_r($this->fm->params);
        $tpl = $this->fm->getTPL();


        $id_sprawy = $this->fm->params[0];
        $akcja = $this->fm->params[1];

        //tak czy siak dodaje komentarze
        //$modsprawy->dodajKoments($id_sprawy, 10, $_SESSION[komentarze]);
        //unset($_SESSION[komentarze]);

        switch ($akcja) {
            case 'przyjmij':
                $modsprawy->przyjmij($id_sprawy);
                break;
            case 'odrzuc':
                $modsprawy->odrzuc($id_sprawy);
                break;
            case 'doopracowania':
                $modsprawy->stan($id_sprawy, 30);
                break;
            case 'dosprawdzenia':
                $modsprawy->stan($id_sprawy, 40);
                header("Location: /index/start");
                break;
            case 'doakceptacji':
                $modsprawy->stan($id_sprawy, 50);
                header("Location: /index/start");
                break;
            case 'dorealizacji':
                $modsprawy->stan($id_sprawy, 60);
                break;
            case 'dooceny':
                //pobieram dzialania:
                $sprawa[data_realizacji] = "2012-01-01";
                $dzialania = $modsprawy->getDzialania($id_sprawy, FALSE);
                foreach ($dzialania as $d) {
                    if ($d[czas_f] > $sprawa[data_realizacji])
                        $sprawa[data_realizacji] = $d[czas_f];
                }
                $modsprawy->update($sprawa, $id_sprawy);
                $modsprawy->stan($id_sprawy, 70);
                break;
            case 'zakoncz':
                $modsprawy->stan($id_sprawy, 80);
                break;
            default:
                break;
        }


        header("Location: /sprawa/show/$id_sprawy");
    }

    function dodajdzialanie() {

        $modsprawy = $this->fm->loadModel('Sprawy');
        //print_r($this->fm->params);
        //$tpl = $this->fm->getTPL();


        $id_sprawy = $this->fm->params[0];
        $id_typu = $this->fm->params[1];

        if (!is_numeric($id_typu))
            $id_typu = 1;

        //tak czy siak dodaje komentarze
        $dzialanie = $_REQUEST[dzialanie];
        $dzialanie[typ_dzialania] = $id_typu;

        $modsprawy->dodajDzialanie($id_sprawy, $dzialanie);

        echo '<i style="color:green">Poprawnie dodano działanie</i>';
    }

    function getdzialania() {

        $modsprawy = $this->fm->loadModel('Sprawy');
        //print_r($this->fm->params);
        $tpl = $this->fm->getTPL();
        $id_sprawy = $this->fm->params[0];
        $id_typu = $this->fm->params[1];
        if (!is_numeric($id_typu))
            $id_typu = 1;
        $dzialania = $modsprawy->getDzialania($id_sprawy, $id_typu);
        $tpl->assign('dzialania', $dzialania);
        $tpl->display('lista_dzialan.html');
    }

    function getdzialanie() {
        $modsprawy = $this->fm->loadModel('Sprawy');
        //print_r($this->fm->params);
        $tpl = $this->fm->getTPL();
        $id_dzialania = $this->fm->params[0];

        if (is_numeric($id_dzialania)) {
            $dzialanie = $modsprawy->getDzialanie($id_dzialania);
            //print_r($dzialanie);
            $tpl->assign('dzialanie', $dzialanie);
        }
        $tpl->display('dzialanie_edit.html');
    }

    function updatedzialanie() {
        $id = $this->fm->params[0];
        //print_r($_REQUEST); exit(); 
        $modsprawy = $this->fm->loadModel('Sprawy');
        $dzialanie = $_REQUEST[dzialanie];
        $modsprawy->updateDzialanie($dzialanie, $id);
    }

    function updateDzialania() {
        $id_sprawy = $this->fm->params[0];
        //print_r($_REQUEST); exit(); 
        $modsprawy = $this->fm->loadModel('Sprawy');

        $dzialania = $_REQUEST[dzialania];

        foreach ($dzialania as $id => $dzialanie) {

            if ($dzialanie[czas_tmp])
                $dzialanie[czas_f] = date('Y-m-d', strtotime($dzialanie[czas_tmp]));
            unset($dzialanie[czas_tmp]);
            $modsprawy->updateDzialanie($dzialanie, $id);
        }
        header("Location: /sprawa/show/$id_sprawy");
    }

    function ocen() {

        $fc = new Fronted($this->fm);
        $fc->auth();

        $modsprawy = $this->fm->loadModel('Sprawy');
        $id_sprawy = $this->fm->params[0];

        ///////////////////////////////////////

        $niezgodnosc = $_POST[niezgodnosc];
        
       // print_r($_POST);

        if ($niezgodnosc) {
            $niezgodnosc[id_stanu] = 80;
            $modsprawy->update($niezgodnosc, $id_sprawy);
        }
        header("Location: /sprawa/show/$id_sprawy");
    }

}