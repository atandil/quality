<?php
namespace App\Main\Controller;

/**
 * Description of Fronted
 *
 * @author Damian
 */
class Fronted extends \SandS\DefaultControler {
    
    function auth() {
        $auth=$this->fm->getAuth();
        if(!$auth->isLogged) $auth->login();
    }
    
    function show($template) {
        $tpl = $this->fm->getTPL();
        $tpl->setTemplate($template,'main');
        $tpl->show();
        
    }
    
    
    
    
}
