<?php

namespace App\Main\Controller;

class IndexController extends \SandS\DefaultControler {

    public function index() {
        $fc = new Fronted($this->fm);
        //$fc->auth()
        $fc->show('default.html');
    }
    
    public function start() {
        $fc = new Fronted($this->fm);
        $fc->auth();
        
        
        //sprawdzam
        if ($_SESSION[logged] == 'administrator') {
            $fc->show('start_administrator.html');
        } else {
            $fc->show('start.html');
        }
    }
    
    
    public function addproblem() {
        $fc = new Fronted($this->fm);
        $modmail =new \App\Main\Model\Mail($this->fm);
        
        $zgloszenie=$_REQUEST[zgloszenie];
        
        
        $text= "Użytkownik ".$_SESSION[pracownik][imie_p]."  ".$_SESSION[pracownik][nazwsko_p]." \n"
                ."w roli ".$_SESSION[pracownik][rola]." \n"
                ."zgłosił problem \n"
                . "Tytul: $zgloszenie[tytul] "
                . "$zgloszenie[tresc]";
        
       $modmail->send("Nowy problem", $text, "", "");
        
        
    }

}
