<?php

/**
 * Description of Szblon
 *
 * @author Damian
 */

namespace App\Main\Controller;

class SzablonController {

    private $fm;

    public function __construct($fm) {
        $this->fm = $fm;
    }

    public function index() {
       
        $path= str_replace('/szablon/', 'szablony/',$_SERVER["REQUEST_URI"]);
        
        //echo $path;
        if(!$path) $path='default.html';
        
        
        $tpl=$this->fm->getTPL();
        
        $tpl->setTemplate($path,'main');
        
        $tpl->show();
        
        
    }

}

