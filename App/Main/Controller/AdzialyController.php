<?php

namespace App\Main\Controller;

use App\Main\Model;

/**
 * Description of NiezgodnosciController
 *
 * @author Damian
 */
class AdzialyController extends \SandS\DefaultControler {

    function index() {
        $fc = new Fronted($this->fm);
        $fc->auth();
        ///echo "aaa";
    }

    function getedycja() {
        $id_pracownika = $this->fm->params[0];
        $modprac = new Model\Pracownicy($this->fm);
        if ($id_pracownika)
            $pracownik = $modprac->get($id_pracownika);
        $dzialy = $modprac->dzialy();

        $tpl = $this->fm->getTPL();
        $tpl->assign('dzialy', $dzialy);
        $tpl->assign('pracownik', $pracownik);
        $tpl->display('1_employee_add_edit.html');
    }

    function update() {
        $id = $this->fm->params[0];
        //print_r($_REQUEST); exit(); 
        $modprac = $this->fm->loadModel('Pracownicy');
        $pracownik = $_REQUEST[pracownik];
        $modprac->update($pracownik, $id);
    }

    function add() {
        //print_r($_REQUEST); exit(); 
        $modprac = $this->fm->loadModel('Pracownicy');
        $pracownik = $_REQUEST[pracownik];
        $modprac->add($pracownik);
    }

    function lista() {
        $fc = new Fronted($this->fm);
        $fc->auth();
        $tpl = $this->fm->getTPL();
        $modprac = $this->fm->loadModel('Pracownicy');
        $dzialy = $modprac->dzialy();
        $tpl->assign('dzialy', $dzialy);
        $tpl->setTemplate('1_dzialy_list.html', 'main');
        $tpl->show();
    }
    
    function listaIn() {
        //$fc->auth();
        $tpl = $this->fm->getTPL();
        $modprac = $this->fm->loadModel('Pracownicy');
        $pracownicy = $modprac->lista();
        $tpl->assign('pracownicy', $pracownicy);
        $dzialy = $modprac->dzialy();
        $tpl->assign('dzialy', $dzialy);
        $tpl->display('1_employee_list.html');
    }

    function nowy() {
        $fc = new Fronted($this->fm);
        $fc->auth();
        $tpl = $this->fm->getTPL();
        $modprac = $this->fm->loadModel('Pracownicy');

        //dluga forma
        $dzialy = $modprac->dzialy();
        //print_r($dzialy); //pozwala wyswietlic to co jest przypisane do zmiennej
        $tpl->assign('dzialy', $dzialy);

        //komentarz kr�tka forma przypisania
        $tpl->assign('zaklady', $modprac->zaklady());
        $tpl->setTemplate('1_employee_add_edit.html', 'main');
        $tpl->show();
    }

}
