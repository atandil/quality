<?php

namespace App\Main\Controller;

/**
 * Description of NiezgodnosciController
 *
 * @author Damian
 */
class KomentarzeController extends \SandS\DefaultControler {

    function index() {

        $fc = new Fronted($this->fm);

        $fc->auth();


        $tpl = $this->fm->getTPL();

        $tpl->setTemplate('start.html', 'main');
        $tpl->show();
    }

    function dodaj() {
        $modsprawy = $this->fm->loadModel('Sprawy');
        $tpl = $this->fm->getTPL();

        // echo "<pre>";
        //print_r($_SESSION);
        $komentarz[komentarz] = $_REQUEST[pole];
        $komentarz[id_pracownika] = $_SESSION[pracownik][id_pracownika];
        $komentarz[data_kom] = date("Y-m-d H:i:s");


        $id_sprawy = $this->fm->params[0];
        $id_stanu = $this->fm->params[1];
        
        $krok=1;
        if ($id_stanu > 20)
            $krok = 2;
        if ($id_stanu > 50)
            $krok = 3;
        if ($id_stanu > 60)
            $krok = 4;

        if ($id_sprawy) {
            
            $modsprawy->dodajKoment($id_sprawy,$id_stanu,$komentarz);
            $komentarze=$modsprawy->getKoment($id_sprawy,$krok);
            $tpl->assign('komentarze', $komentarze);
        } else {
            $_SESSION[komentarze][] = $komentarz;
            
            $tpl->assign('komentarze', $_SESSION[komentarze]);
        }
        $tpl->display('komentarze_lista_krok.html');

        //$fc->show('niezgodnosc_new.html');
    }

}