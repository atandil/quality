#SandS
* Version: 1.0.0
* [Website]
http://quality.barczyk.info
* [Release Documentation]

## Description


## More information

Wymaganie system PHP 5.4

1. standardy tworzenia kodu zgodne z PSR-2  

http://www.tarnaski.eu/blog/php-i-standardy-psr/

    Stałe klas muszą być pisane tylko dużymi literami a osobne wyrazy oddzielamy podkreśleniem.
    Nazwy klas muszą być w formie StudlyCaps czyli wszystkie wyrazy bez przerw i z dużych liter.
    Metody klas muszą być pisane w standardzie camelCase().
    standard nazywania zmiennych $under_score

2.  Katalogi
/App  --- Aplikacje
    /Main
        /config
         /Controler
         /Model
         /Viev
/core  - podstawowe pliki frameworka
    /SandS - głowne klasy
     /init.php
     /autoload.php

/config  - pliki konfiguracyjne w formacje YALM
/vendor  - pliki bibliotek zewnętrznych
    /smarty
    /adodb
    /PEAR

/public  - to co jest widoczne dla użytkownikow  (użytkownicy NIE MAJĄ dostępu do silnika )

/tmp  - pliki tymczasowe
    /templates_c
   /cache

3. Interfejsy
Konlorery musza implementowac  interfejs \SandS\IContoler
Moduły musza implementować Interfejs \SandS\IModel


##Development Team

* Damian Barczyk - Project Manager, Developer

