var AJAXBLAD = "ajaxerror";
var MYSZY;
var MYSZX;

function inout(akc)
{
    $('#div_inout').stop(true, true).remove();
    $('#div_inouttlo').stop(true, true).remove();
    if (akc == 1)
    {
        var sztlo = '<div id="div_inouttlo" style="display:none; z-index:10001; position:absolute; cursor:progress; background-color:transparent; left:0px; top:0px; margin:0px 0px;"></div>';
        var szablon = sztlo + '<div id="div_inout" style="display:none; z-index:10002;"></div>';
        $("body").prepend(szablon);
        $('#div_inouttlo').css({'width': +$(document).find('body').width(), 'height': $(document).find('body').height()}).fadeTo('fast', 0.07);
        $('#div_inout').css({'margin-left': MYSZX + 10, 'margin-top': MYSZY - 10}).show();
    }
}

function login(fr)
{
    var bladdlug = AJAXBLAD.length;
    $.post($(fr).attr('action'), $(fr).serialize(), function(odp)
    {
        var odp2 = $.trim(odp).substring(0, bladdlug);
        if (odp2 == AJAXBLAD)
            alertt($.trim(odp).substring(bladdlug))
        else
            location.href = odp;
    });
}

function skrol(obj_id, pod, kompens)
{
    /* jezeli pod==1, przewinac pod spod obiektu obj_id, dodac kompens px do paska scroll */
    /* jezeli pod==1, przewinac pod spod obiektu obj_id, dodac kompens px do paska scroll */
    if (obj_id == 'mm_header' || !$('#' + obj_id).size() > 0)
    {
        obj_id = 'bdd';
        kompens = 0;
    }
    var obj = $('#' + obj_id);
    var roznica = 0;
    var ofset = $(obj).offset();
    if (pod)
        roznica = parseInt((ofset.top - $(obj).height()))
    else
        roznica = parseInt(ofset.top);
    roznica = roznica > 0 ? roznica : 0;
    if (!isNaN(kompens))
        roznica = parseInt(roznica + kompens);
    $('html, body').animate({scrollTop: roznica + 'px'}, 'normal', 'linear');
}

function wykonaj(adres, cel, param, pole_value)
{
    $.post(adres, {pole: param}, function(odp)
    {
        if (pole_value == 1)
            $('#' + cel).val(odp)
        else
            $('#' + cel).fadeTo(300, 0.1, function()
            {
                $(this).html(odp).fadeTo(300, 1);
            });
    });
}

function ladujfast(adres, cel)
{
    $.post(adres,  function(odp)
    {
      $('#' + cel).html(odp);
    });
}

function wykonajalertt(adres, cel, param, pole_value)
{
    var bladdlug = AJAXBLAD.length;
    $.post(adres, {pole: param}, function(odp)
    {
        var odp2 = $.trim(odp).substring(0, bladdlug);
        if (odp2 == AJAXBLAD)
            alertt($.trim(odp).substring(bladdlug), '', '', 1)
        else if (cel)
        {
            if (pole_value == 1)
                $('#' + cel).val(odp)
            else
                $('#' + cel).html(odp);
        }
    });
}

function wysform(form_id, cel, cel_typ)
{
    var czas = 400;
    var bladdlug = AJAXBLAD.length;
    $.post($('#' + form_id).attr('action'), $('#' + form_id).serialize(), function(odp)
    {
        var odp2 = $.trim(odp).substring(0, bladdlug);
        if (odp2 == AJAXBLAD)
            alertt($.trim(odp).substring(bladdlug))
        else if (cel)
        {
            if (cel_typ == 'text')
                $('#' + cel).fadeOut(czas, function() {
                    $(this).text($.trim(odp)).fadeIn(czas);
                })
            else if (cel_typ == 'value')
                $('#' + cel).fadeOut(czas, function() {
                    $(this).val($.trim(odp)).fadeIn(czas);
                })
            else
                $('#' + cel).fadeOut(czas, function() {
                    $(this).html($.trim(odp)).fadeIn(czas);
                })
        }
    });
}

function wykform(form_id, adres, cel)
{
    var czas = 400;
    $.post(adres, $('#' + form_id).serialize(), function(odp)
    {
        if (cel)
        {
            $('#' + cel).fadeOut(czas, function() {
                $(this).html($.trim(odp)).fadeIn(czas);
            });
        }
    });
}
function wykformObj(form, adres, cel)
{
    var czas = 400;
    $.post(adres, form.serialize(), function(odp)
    {
        if (cel)
        {
            $('#' + cel).fadeOut(czas, function() {
                $(this).html($.trim(odp)).fadeIn(czas);
            });
        }
    });
}


function wysform2(form_id, cel, cel_typ)
{	/*  BEZ ANIMACJI, krotki czas, bez obslugi komunikatu */
    var czas = 50;
    $.post($('#' + form_id).attr('action'), $('#' + form_id).serialize(), function(odp)
    {
        if (cel_typ == 'text')
            $('#' + cel).fadeOut(czas, function() {
                $(this).text($.trim(odp)).fadeIn(czas);
            })
        else if (cel_typ == 'value')
            $('#' + cel).fadeOut(czas, function() {
                $(this).val($.trim(odp)).fadeIn(czas);
            })
        else
            $('#' + cel).fadeOut(czas, function() {
                $(this).html($.trim(odp)).fadeIn(czas);
            })
    });
}


function wysform3(form_id, cel, cel_typ)
{
    /* usuwa ewentualne okno komunikatu */
    var czas = 400;
    var bladdlug = AJAXBLAD.length;
    $('#submit_alertt').slideUp(100, function() {
        $(this).html('');
    });
    $.post($('#' + form_id).attr('action'), $('#' + form_id).serialize(), function(odp)
    {
        var odp2 = $.trim(odp).substring(0, bladdlug);
        if (odp2 == AJAXBLAD && bladdlug > 0)
            alertt2($.trim(odp).substring(bladdlug))
        else if (cel)
        {
            if (cel_typ == 'text')
                $('#' + cel).fadeTo(czas, 0.1, function() {
                    $(this).text($.trim(odp)).fadeTo(czas, 1);
                })
            else if (cel_typ == 'value')
                $('#' + cel).fadeTo(czas, 0.1, function() {
                    $(this).val($.trim(odp)).fadeTo(czas, 1);
                })
            else
                $('#' + cel).fadeTo(czas, 0.1, function() {
                    $(this).html($.trim(odp)).fadeTo(czas, 1);
                })
        }
    });
}


function podpowiedzi(adres, wart, cel)
{
    var czas = 400;
    var bladdlug = AJAXBLAD.length;
    $.post(adres, {pole: wart}, function(odp)
    {
        var odp2 = $.trim(odp).substring(0, bladdlug);
        if (odp2 == AJAXBLAD)
            alertt($.trim(odp).substring(bladdlug))
        else if (cel)
        {
            if (odp.length > 0)
                $('#' + cel).slideUp(czas, function() {
                    $(this).html($.trim(odp)).slideDown(czas);
                })
            else
                $('#' + cel).slideUp(czas).html('');
        }
    });
}

function porownaj_liczby(lmin, lmax)
{
    if (lmin && lmax)
        if (parseInt(lmin) > parseInt(lmax))
            return false;
    return true;
}

function porownaj_daty(dmin, dmax, delimiter)
{
    var d1 = unix_data(dmin, delimiter);
    var d2 = unix_data(dmax, delimiter);
    return porownaj_liczby(d1, d2);
}

function unix_data(dataa, delimiter)
{
    /* zwraca unix_timestamp */
    if (!delimiter)
        delimiter = '-';
    var wynik = 0;
    if (dataa)
    {
        var arr = dataa.split(delimiter);
        if (arr.length == 3)
        {
            if (arr[0].length == 4) /* zaczyna sie od roku */
                wynik = new Date(arr[0], arr[1], arr[2])
            else  /* konczy sie rokiem */
                wynik = new Date(arr[2], arr[1], arr[0]);
            wynik = wynik.getTime();
            if (isNaN(wynik))
                wynik = 0;
        }
    }
    return wynik;
}


function alertt(txt, pole_z_txt, szer)
{
    if ($('#div_alertt').size() > 0)
        $('#div_alertt').remove();
    /* pobrac tekst z podanego selectora lub wstawic tekst z przekazanego parametru */
    var tekst = !txt && pole_z_txt ? $("#" + pole_z_txt).html() : txt;
    var szablon = '<div id="div_alertt" style="display:none; z-index:10001;"><div id="alertttlo"><div id="alertt_txt">' + tekst + '</div><button id="alertt_butt">Zamknij</button></div></div>';
    $("body").prepend(szablon);
    $('#div_alertt li').addClass('MM_li');
    /* jezeli podano szerokosc - ustawic */
    if (parseInt(szer) > 0)
        $('#div_alertt').width(parseInt(szer));

    $('#alertt_butt').click(function() {
        $('#div_alertt').hide('normal', function() {
            $(this).remove();
        });
    })
    if (wysrodkujj($('#div_alertt'), $('#div_alertt').height()))
        $('#div_alertt').css({'left': '50%', 'margin-left': -($('#div_alertt').width() / 2) + 'px'}).show('middle');
}

function wysrodkujj(ob, wys)
{
    if ($(ob))
    {
        var margtop = 0;
        if (!wys)
            wys = $(ob).height()
        else
            $(ob).height(wys + 'px');
        margtop = parseInt(($(window).height() / 2) - (wys / 2) + $(window).scrollTop());
        $(ob).css({'margin-top': margtop + 'px'});
        return true;
    }
    return false;
}



function wysrodkuj(ob, szer, wys)
{
    if ($(ob))
    {
        var margtop = 0;
        if (szer > 0)
            $(ob).width(szer + 'px');
        if (!wys)
            wys = $(ob).height();
        if (wys > 0)
        {
            $(ob).height(wys + 'px');
            margtop = (($(window).height() / 2) - ($(ob).height() / 2) + $(window).scrollTop());
        }
        else
            margtop = ($(window).scrollTop() + 160);
        $(ob).css({'margin-left': (($(window).width() / 2) - ($(ob).width() / 2)) + 'px', 'margin-top': margtop + 'px'});
    }
    return true;
}



function alertt2(txt, pole_z_txt, szer)
{
    var ALERT_SZER = 400
    if (parseInt(szer) > 0)
        ALERT_SZER = szer;
    var tekst = '';
    if (!txt && pole_z_txt)
        tekst = $("#" + pole_z_txt).html()
    else
        tekst = txt;
    var szablon = '<div id="div_alertt" style="display:none; z-index:10001;"><div id="alertttlo"><div id="alertt_txt">' + tekst + '</div><button id="alertt_butt">Zamknij</button></div></div>';
    $("#center").prepend(szablon);
    $('#div_alertt').find("li").addClass('MM_li');
    $('#alertt_butt').click(function() {
        $('#div_alertt').hide('middle', function() {
            $(this).remove();
        });
    });
    if (wysrodkuj2($('#center'), $('#div_alertt')))
        $('#div_alertt').slideDown('middle');
}

function wysrodkuj2(rodz, obj)
{
    if ($(rodz) && $(obj))
        $(obj).css({'margin-left': (($(rodz).width() / 2) - ($(obj).width() / 2)) + 'px', 'margin-top': (($(rodz).height() / 2) - ($(obj).height() / 2)) + 'px'});
    return true;
}



function akordeon(uk, po, ukc, poc, pole_pt, pt)
{
    /* pt EWENTUALNY TEKST DO ZAMIANY */
    var uc = ukc > 0 ? ukc : 500;
    var pc = poc > 0 ? poc : 500;
    var flaga = 0;

    if (uk)
        $(uk).each(function() {
            if ($(this).filter(':visible').size() > 0)
                flaga = 1;
        });

    if (flaga == 1)
        $(uk).not(po).slideUp(uc, function()
        {
            $(pole_pt).html(pt);
            $(po).slideDown(pc);
        });
    else
    {
        $(pole_pt).html(pt);
        $(po).slideDown(pc);
    }
}


function akordeon2(uk, po, ukc, poc, pole_pt, pt)
{
    /* pt EWENTUALNY TEKST DO ZAMIANY */
    /* akordeony roznia sie efektem  */
    var uc = ukc > 0 ? ukc : 500;
    var pc = poc > 0 ? poc : 500;
    var flaga = 0;

    if (uk)
        $(uk).each(function() {
            if ($(this).filter(':visible').size() > 0)
                flaga = 1;
        });

    if (flaga == 1)
        $(uk).not(po).hide(uc, function()
        {
            $(pole_pt).html(pt);
            $(po).show(pc);
        });
    else
    {
        $(pole_pt).html(pt);
        $(po).show(pc);
    }
}


function tabs(uk, div_pt, uk_sel, obj_sel, ptxt)
{
    /* div_pt do pokazania */
    /* ptxt ewent.tekst do zamiany w polu div_pt */
    /* uk_sel grupa, z ktorej zdjac selected i zaznaczyc obj_sel */
    $(uk).css('display', 'none');
    $(uk_sel).removeClass('selected');
    if (ptxt)
        $(div_pt).html(ptxt);
    $(div_pt).css('display', 'block');
    $(obj_sel).addClass('selected');
}


function MMtree(obj)
{
    var klasa = 'wybrany';
    $(obj).parent().find('li').removeClass(klasa);
    $(obj).addClass(klasa);
}

function pimg(obj, ukr)
{
    if (obj)
    {
        if (!ukr) /* pokaz */
        {
            var mrgn = parseInt((990 - $('#' + obj).width()) / 2) + 'px';
            $('#' + obj).fadeTo(0, 0, function()
            {
                $(this).clone().appendTo('#ct').css(
                        {
                            'width': 'auto',
                            'height': 'auto',
                            'margin-top': '5px',
                            'margin-left': mrgn,
                            'margin-right': mrgn,
                            'top': parseInt($(window).scrollTop())
                        }).fadeTo(400, 1);
            });
        }
        else  /* zniszcz */
            $(obj).fadeOut(400, function() {
                $(this).remove();
            });
    }
}


function cu()
{
    var odp = confirm('Czy usunąć wybraną pozycję ?..');
    if (odp)
        return true
    else
        return false;
}

function anogl()
{
    var odp = confirm('Czy chcesz przerwać dodawanie nowego ogłoszenia ?...');
    if (odp)
        return true
    else
        return false;
}

function cw(txt)
{
    var tekst = "Czy masz pewność, że chcesz wykonać tę operację ?...\r\n";
    if (txt)
        tekst = tekst + txt;
    var odp = confirm(tekst);
    if (odp)
        return true
    else
        return false;
}



function licz_znaki(maks, txt_id, bar_id, info_id)
{
    var bg_col = '#ee0000';
    var pole = $('#' + txt_id);
    var pole_ile = $(pole).val();
    pole_ile = pole_ile.length;
    if (!maks > 0)
        maks = 1800;
    var pozostalo = (maks - pole_ile);
    var pole_dlug = $(pole).width();
    if (pole_ile > maks)
    {
        $(pole).val($(pole).val().substring(0, maks));
    }
    else
    {
        var proc = parseInt(100 - ((pozostalo) * 100) / maks);
        $('#' + bar_id).height('3px').width(parseInt((pole_dlug * proc) / 100) + "px").css('background-color', bg_col);
        $('#' + info_id).html('Pozostało: ' + (pozostalo) + ' znaków.');
    }
}

function wb(idog, cel, akc)
{
    /* wybrane ogloszenia */
    if (!akc)
    {
        var adres = '/ogloszenia/ogloszenia/wybrane/' + idog;
        $('#imgwybr' + idog).attr('src', '/images/new/obserwuj_1.png');
        $('#imgwybr' + idog).parent().find('span').attr('class', 'kol_czer');
    }
    else
    {
        var adres = '/ogloszenia/ogloszenia/wybranedel/' + idog;
        $('#imgwybr' + idog).attr('src', '/images/new/obserwuj_0.png');
        $('#imgwybr' + idog).parent().find('span').attr('class', 'kol_ziel2');
    }

    $.getJSON(adres, function(odp)
    {
        if (odp.ile > 0)
        {
            $('#' + cel).html(odp.ogloszenia);
            $('#spanwybile').html(odp.ile);
        }
        else
        {
            $('#' + cel).html('<table><thead><tr><th class="MM_td kol_braz">Brak obserwowanych ogłoszeń...</th></tr></thead></table>');
            $('#spanwybile').html('0');
        }
    });
}

function sprpowod()
{
    if ($("#id_powod option:eq(0)").is(":selected"))
    {
        $('#powoderror').show();
        return false;
    } else
        return true;
}



function ustikone(obj, szer, wys)
{
    /* dopasowauje rozm miniatury na liscie ogl */
    if ($(obj).width() == $(obj).height())
        $(obj).height(wys)
    else if ($(obj).width() > $(obj).height())  /* poziome */
        $(obj).width(szer)
    else if ($(obj).width() < $(obj).height())  /* pionowe */
        $(obj).height(wys);
    else
    {
        $(obj).width(szer);
        $(obj).height(wys);
    }
    $(obj).css('display', 'block');
}
