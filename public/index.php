<?php
/**
 * Website document root
 */
define('DOC_ROOT', __DIR__.DIRECTORY_SEPARATOR);
/**
 * Base framework path directory.
 */
define('BASE_PATH', realpath(__DIR__.'/..').DIRECTORY_SEPARATOR);

/**
 * Base Initlalize
 */
require BASE_PATH.'core/init.php';


/**
 * Autoloader
 */
require 'autoload.php';

/**
 * Starting Framework
 */
$fm = new SandS\SandS();
$fm->run();

